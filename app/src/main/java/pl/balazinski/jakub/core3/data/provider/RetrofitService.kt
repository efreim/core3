package pl.balazinski.jakub.core3.data.provider

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.balazinski.jakub.core3.data.model.Model
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    companion object {
        fun create(): RetrofitService {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()

            return retrofit.create(RetrofitService::class.java)
        }
    }

    @GET("/posts")
    fun getPosts(): Observable<List<Model.Post>>

    @GET("/users")
    fun getAuthor(@Query("id") userId: Int): Observable<List<Model.User>>

    @GET("/comments")
    fun getComments(@Query("postId") postId: Int): Observable<List<Model.Comment>>
}