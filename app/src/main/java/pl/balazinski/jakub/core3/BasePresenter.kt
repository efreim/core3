package pl.balazinski.jakub.core3

interface BasePresenter {
    fun destroy()
    fun loadData()

}