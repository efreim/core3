package pl.balazinski.jakub.core3.post_content

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.balazinski.jakub.core3.data.model.Model
import pl.balazinski.jakub.core3.data.provider.DataProvider
import timber.log.Timber

class PostContentPresenter(private val postContentView: PostContentContract.View) :
    PostContentContract.Presenter {

    private lateinit var post: Model.Post
    init {
        postContentView.presenter = this
    }

    private var disposable: Disposable? = null

    override fun init(post: Model.Post) {
        postContentView.showPostView(post)
        this.post = post
    }

    override fun loadComments(postId: Int) {
        postContentView.showProgressBar()
        disposable =
                DataProvider.getComments(postId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            postContentView.run {
                                showCommentsView(result)
                                hideProgressBar()
                            }
                        },
                        { error ->
                            postContentView.run {
                                Timber.d(error.message)
                                showErrorView(this@PostContentPresenter)
                                hideProgressBar()
                            }
                        }
                    )
    }

    override fun loadAuthor(authorId: Int, postId: Int) {
        postContentView.showProgressBar()
        disposable =
                DataProvider.getAuthor(authorId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            postContentView.run {
                                //it's api fault that getting single user returns list of one user
                                showAuthorView(result[0], postId)
                                hideProgressBar()
                            }
                        },
                        { error ->
                            postContentView.run {
                                Timber.d(error.message)
                                showErrorView(this@PostContentPresenter)
                                hideProgressBar()
                            }
                        }
                    )
    }

    override fun onAuthorClicked(email: String, authorId: Int, postId: Int) {
        postContentView.showEmailChooser(email, authorId, postId)
    }

    override fun destroy() {
        disposable?.dispose()
    }

    override fun loadData() {
        loadAuthor(post.userId, post.id)
        loadComments(post.id)

    }
}