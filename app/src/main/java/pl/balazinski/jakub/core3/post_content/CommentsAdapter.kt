package pl.balazinski.jakub.core3.post_content


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_comment_item.view.*
import pl.balazinski.jakub.core3.R
import pl.balazinski.jakub.core3.data.model.Model

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder>() {

    var comments: List<Model.Comment> = emptyList()

    class CommentsViewHolder(val relativeLayout: RelativeLayout) : RecyclerView.ViewHolder(relativeLayout)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CommentsViewHolder {
        val relativeLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_comment_item, parent, false) as RelativeLayout

        return CommentsViewHolder(relativeLayout)
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        holder.relativeLayout.body_text_view.text = comments[position].body
        holder.relativeLayout.name_text_view.text = comments[position].name
        holder.relativeLayout.email_text_view.text = comments[position].email
    }

    override fun getItemCount() = comments.size
}