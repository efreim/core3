package pl.balazinski.jakub.core3.post_content

import pl.balazinski.jakub.core3.BasePresenter
import pl.balazinski.jakub.core3.BaseView
import pl.balazinski.jakub.core3.data.model.Model

interface PostContentContract {

    interface View : BaseView<Presenter> {
        fun showCommentsView(comments: List<Model.Comment>)
        fun showPostView(post: Model.Post)
        fun showAuthorView(author: Model.User, postId: Int)
        fun showEmailChooser(email: String, authorId: Int, postId: Int)
    }

    interface Presenter : BasePresenter {
        fun init(post: Model.Post)
        fun loadComments(postId: Int)
        fun loadAuthor(authorId: Int, postId: Int)
        fun onAuthorClicked(email: String, authorId: Int, postId: Int)
    }
}