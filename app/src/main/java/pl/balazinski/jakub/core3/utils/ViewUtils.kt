package pl.balazinski.jakub.core3.utils

import android.app.Activity
import android.app.AlertDialog
import pl.balazinski.jakub.core3.BasePresenter
import pl.balazinski.jakub.core3.R

class ViewUtils(private val ctx: Activity) {

    fun createAndShowDialog(retry: BasePresenter) {
        AlertDialog.Builder(ctx).run {
            setTitle(R.string.error_title)
            setMessage(R.string.error_body)
            setPositiveButton(context.resources.getText(R.string.error_positive)) { dialog, _ -> retry.loadData(); dialog.dismiss() }
            setNegativeButton(context.getString(R.string.error_negative)) { dialog, _ ->
                dialog.dismiss(); ctx.finish()
            }
                .show()
        }
    }
}