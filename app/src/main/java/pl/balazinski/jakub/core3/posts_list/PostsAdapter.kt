package pl.balazinski.jakub.core3.posts_list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_post_item.view.*
import pl.balazinski.jakub.core3.R
import pl.balazinski.jakub.core3.data.model.Model

class PostsAdapter(private val itemListener: PostsListActivity.PostItemListener) :
    RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {

    var posts: List<Model.Post> = emptyList()

    class PostsViewHolder(val relativeLayout: RelativeLayout) : RecyclerView.ViewHolder(relativeLayout)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PostsViewHolder {
        val relativeLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_post_item, parent, false) as RelativeLayout

        return PostsViewHolder(relativeLayout)
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.relativeLayout.title_text_view.text = posts[position].title
        holder.relativeLayout.body_text_view.text = posts[position].body
        holder.relativeLayout.setOnClickListener {
            itemListener.onPostClicked(
                posts[position],
                holder.relativeLayout.context
            )
        }
    }

    override fun getItemCount() = posts.size
}