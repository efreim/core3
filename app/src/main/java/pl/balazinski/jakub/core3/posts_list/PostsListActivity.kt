package pl.balazinski.jakub.core3.posts_list

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_posts_list.*
import pl.balazinski.jakub.core3.BasePresenter
import pl.balazinski.jakub.core3.R
import pl.balazinski.jakub.core3.data.analytics.Analytics
import pl.balazinski.jakub.core3.data.model.Model
import pl.balazinski.jakub.core3.post_content.PostContentActivity
import pl.balazinski.jakub.core3.utils.ViewUtils
import pl.balazinski.jakub.core3.utils.runLayoutAnimation

class PostsListActivity : Activity(), PostsListContract.View {

    override lateinit var presenter: PostsListContract.Presenter

    private lateinit var recyclerViewAdapter: PostsAdapter

    private val analytics = Analytics()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts_list)
        posts_recycler_view.layoutManager = LinearLayoutManager(this)
        recyclerViewAdapter = PostsAdapter(postItemListener)
        posts_recycler_view.adapter = recyclerViewAdapter

        analytics.onPostsListSeen(this)

        presenter = PostsListPresenter(this)
        presenter.loadPosts()
    }


    override fun showErrorView(presenter: BasePresenter) {
        ViewUtils(this).createAndShowDialog(presenter)
    }

    override fun showProgressBar() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progress_bar.visibility = View.GONE
    }

    override fun onResume() {
        posts_recycler_view.runLayoutAnimation()
        super.onResume()
    }

    override fun showPostsList(posts: List<Model.Post>) {
        recyclerViewAdapter.posts = posts
        posts_recycler_view.runLayoutAnimation()
    }

    override fun showPostView(post: Model.Post) {
        PostContentActivity.start(this, post)
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    interface PostItemListener {

        fun onPostClicked(clickedPost: Model.Post, context: Context)

    }

    private var postItemListener: PostItemListener = object : PostItemListener {
        override fun onPostClicked(clickedPost: Model.Post, context: Context) {
            presenter.onPostClicked(clickedPost)
            analytics.onPostsListItemClick(context, clickedPost.id, clickedPost.userId)
        }
    }

}
