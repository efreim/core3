package pl.balazinski.jakub.core3.post_content

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_post_content.*
import pl.balazinski.jakub.core3.BasePresenter
import pl.balazinski.jakub.core3.R
import pl.balazinski.jakub.core3.data.analytics.Analytics
import pl.balazinski.jakub.core3.data.model.Model
import pl.balazinski.jakub.core3.utils.ViewUtils
import pl.balazinski.jakub.core3.utils.runLayoutAnimation

class PostContentActivity : Activity(), PostContentContract.View {

    override lateinit var presenter: PostContentContract.Presenter

    private lateinit var recyclerViewAdapter: CommentsAdapter

    private val analytics = Analytics()

    companion object {
        private const val POST_DATA_KEY = "POST_DATA"

        fun start(context: Activity, post: Model.Post) {
            val intent = Intent(context, PostContentActivity::class.java)
            intent.putExtra(POST_DATA_KEY, post)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_content)
        comments_recycler_view.layoutManager = LinearLayoutManager(this)
        recyclerViewAdapter = CommentsAdapter()
        comments_recycler_view.adapter = recyclerViewAdapter

        actionBar?.setDisplayHomeAsUpEnabled(true)

        val post = intent.getParcelableExtra(POST_DATA_KEY) as Model.Post

        analytics.onPostContentSeen(this, post.id)

        presenter = PostContentPresenter(this)
        presenter.init(post)
        presenter.loadAuthor(post.userId, post.id)
        presenter.loadComments(post.id)
    }

    override fun showCommentsView(comments: List<Model.Comment>) {
        recyclerViewAdapter.comments = comments
        comments_recycler_view.runLayoutAnimation()
    }


    override fun showPostView(post: Model.Post) {
        title_text_view.text = post.title
        content_text_view.text = post.body
    }

    override fun showAuthorView(author: Model.User, postId: Int) {
        author_text_view.run {
            text = context.getString(R.string.author)
            append(author.name)
            setOnClickListener {
                presenter.onAuthorClicked(author.email, author.id, postId)
            }
        }
    }

    override fun showErrorView(presenter: BasePresenter) {
        ViewUtils(this).createAndShowDialog(presenter)
    }

    override fun showProgressBar() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progress_bar.visibility = View.GONE
    }

    override fun showEmailChooser(email: String, authorId: Int, postId: Int) {
        analytics.onAuthorClicked(this, authorId, postId)
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse("mailto:$email")
        startActivity(emailIntent)
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    override fun onNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
