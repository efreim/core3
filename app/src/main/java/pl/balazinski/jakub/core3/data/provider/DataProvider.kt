package pl.balazinski.jakub.core3.data.provider

object DataProvider {

    private val retrofitService by lazy {
        RetrofitService.create()
    }

    fun getPosts() = retrofitService.getPosts()

    fun getAuthor(authorId: Int) = retrofitService.getAuthor(authorId)

    fun getComments(postId: Int) = retrofitService.getComments(postId)
}