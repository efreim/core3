package pl.balazinski.jakub.core3.posts_list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.balazinski.jakub.core3.data.model.Model
import pl.balazinski.jakub.core3.data.provider.DataProvider
import timber.log.Timber

class PostsListPresenter(private val postsListView: PostsListContract.View) :
    PostsListContract.Presenter {

    init {
        postsListView.presenter = this
    }

    private var disposable: Disposable? = null


    override fun loadPosts() {
        postsListView.showProgressBar()
        disposable =
                DataProvider.getPosts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            postsListView.run {
                                showPostsList(result)
                                hideProgressBar()
                            }
                        },
                        { error ->
                            postsListView.run {
                                Timber.d(error.message)
                                showErrorView(this@PostsListPresenter)
                                hideProgressBar()
                            }
                        }
                    )
    }

    override fun onPostClicked(post: Model.Post) {
        postsListView.showPostView(post)
    }

    override fun destroy() {
        disposable?.dispose()
    }

    override fun loadData() {
        loadPosts()
    }
}