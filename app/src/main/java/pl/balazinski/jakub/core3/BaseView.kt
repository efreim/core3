package pl.balazinski.jakub.core3


interface BaseView<T> {
    var presenter: T
    fun showProgressBar()
    fun hideProgressBar()
    fun showErrorView(presenter: BasePresenter)
}