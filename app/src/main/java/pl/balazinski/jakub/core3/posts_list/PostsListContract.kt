package pl.balazinski.jakub.core3.posts_list

import pl.balazinski.jakub.core3.BasePresenter
import pl.balazinski.jakub.core3.BaseView
import pl.balazinski.jakub.core3.data.model.Model

interface PostsListContract {

    interface View : BaseView<Presenter> {
        fun showPostsList(posts: List<Model.Post>)
        fun showPostView(post: Model.Post)
    }

    interface Presenter : BasePresenter {
        fun loadPosts()
        fun onPostClicked(post: Model.Post)
    }
}