package pl.balazinski.jakub.core3.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

object Model {
    @Parcelize
    data class Post(val userId: Int, val id: Int, val title: String, val body: String) : Parcelable
    data class User(val id: Int, val name: String, val username: String, val email: String)
    data class Comment(val postId: Int, val id: Int, val name: String, val email: String, val body: String)
}