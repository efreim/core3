package pl.balazinski.jakub.core3.data.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class Analytics {

    fun onPostsListSeen(context: Context) {
        val bundle = Bundle()
        bundle.putString("screen", "posts_list")
        sendEvent(context, "display", bundle)
    }

    fun onPostsListItemClick(context: Context, postId: Int, authorId: Int) {
        val bundle = Bundle()
        bundle.putString("click", "posts_list_item")
        bundle.putString("postId", postId.toString())
        bundle.putString("authorId", authorId.toString())
        sendEvent(context, "action", bundle)
    }

    fun onPostContentSeen(context: Context, postId: Int) {
        val bundle = Bundle()
        bundle.putString("screen", "post_content")
        bundle.putString("postId", postId.toString())
        sendEvent(context, "display", bundle)
    }

    fun onAuthorClicked(context: Context, postId: Int, authorId: Int) {
        val bundle = Bundle()
        bundle.putString("postId", postId.toString())
        bundle.putString("authorId", authorId.toString())
        sendEvent(context, "action", bundle)
    }

    private fun sendEvent(context: Context, event: String, bundle: Bundle) {
        FirebaseAnalytics.getInstance(context).logEvent(event, bundle)
    }

}